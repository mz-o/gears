import processing.core.PGraphics;
import processing.core.PApplet;
import processing.core.PVector;

class ChainedGearConnectionBase extends GearConnection {
  boolean crossed;
  public ChainedGearConnectionBase(Gear other, Gear g, boolean crossed) {
    super(other, g);
    this.crossed = crossed;
  }
  public void update() {
    if (this.g.getTeethCount()==0) return;
    float speed =  this.other.getSpeed() * ((float)this.other.getTeethCount() / this.g.getTeethCount());
    if (this.crossed) speed*=-1;
    this.g.setSpeed(speed);
  }
  public void drawBefore(PGraphics g) {
    super.drawBefore(g);
    Gear g1, g2;
    float f = 1;
    if (this.other.getRadius() > this.g.getRadius()) {
      g1 = this.other;
      g2 = this.g;
    } else {
      g2 = this.other;
      g1 = this.g;
      f = -1;
    }
    float radio1 = g1.getRadius();
    PVector pos1 = g1.getPosition();
    float centerPositionX1 = pos1.x;
    float centerPositionY1 = pos1.y;
    float rotationSpeed1  = g1.getSpeed();
    float radio2 = g2.getRadius();
    PVector pos2 = g2.getPosition();
    float centerPositionX2 = pos2.x;
    float centerPositionY2 = pos2.y;
    //float rotationSpeed2  = g2.getSpeed();
    float rotationAngle = Gear.rotation;
    float tp11x,tp11y,tp12x,tp12y,tp21x,tp21y,tp22x,tp22y;
    float helpRadius;
    if (this.crossed) {
      helpRadius = radio1+radio2;
    } else {
      helpRadius = radio1-radio2;
    }
    float abstand = PApplet.sqrt((centerPositionX1-centerPositionX2)*(centerPositionX1-centerPositionX2) + (centerPositionY1-centerPositionY2)*(centerPositionY1-centerPositionY2));
    if (abstand==0) return;
    float winkel = PApplet.acos(helpRadius / abstand);
    if (Float.isNaN(winkel)) return; // wenn helpRadius > abstand   dann ist acos nicht definiert
    
    if ((centerPositionX1-centerPositionX2)==0) return;
    float gearWinkel = PApplet.atan((centerPositionY1-centerPositionY2)/(centerPositionX1-centerPositionX2));
    if (centerPositionX1-centerPositionX2 >= 0) {
      gearWinkel = -(PApplet.PI-gearWinkel);
    }
    
    float r,w;
    r = Gear.teeth_height*0.2f;
    g.stroke(50,105,60);
    g.strokeWeight(4);
    w = winkel+gearWinkel;
    
    
    tp11x = centerPositionX1 + (radio1+r)*PApplet.cos(w);
    tp11y = centerPositionY1 + (radio1+r)*PApplet.sin(w);
    if (this.crossed) {
      tp21x = centerPositionX2 + (radio2+r)*PApplet.cos(w+PApplet.PI);
      tp21y = centerPositionY2 + (radio2+r)*PApplet.sin(w+PApplet.PI);
    } else {
      tp21x = centerPositionX2 + (radio2+r)*PApplet.cos(w);
      tp21y = centerPositionY2 + (radio2+r)*PApplet.sin(w);
    }
    w = gearWinkel-winkel;
    tp12x = centerPositionX1 + (radio1+r)*PApplet.cos(w);
    tp12y = centerPositionY1 + (radio1+r)*PApplet.sin(w);
    if (this.crossed) {
      tp22x = centerPositionX2 + (radio2+r)*PApplet.cos(w+PApplet.PI);
      tp22y = centerPositionY2 + (radio2+r)*PApplet.sin(w+PApplet.PI);
    } else {
      tp22x = centerPositionX2 + (radio2+r)*PApplet.cos(w);
      tp22y = centerPositionY2 + (radio2+r)*PApplet.sin(w);
    }
    
    g.line(tp11x, tp11y, tp21x, tp21y);
    g.line(tp12x, tp12y, tp22x, tp22y);
    
    
    g.noFill();
    g.arc(centerPositionX1, centerPositionY1, (radio1+r)*2, (radio1+r)*2,
      (winkel+gearWinkel),
      PApplet.TWO_PI+(-winkel+gearWinkel),
      PApplet.OPEN);
    if (this.crossed) {
      g.arc(centerPositionX2, centerPositionY2, (radio2+r)*2, (radio2+r)*2,
        PApplet.TWO_PI+(gearWinkel-winkel)-PApplet.PI/2,
        PApplet.TWO_PI+(winkel+gearWinkel)+PApplet.PI/2,
        PApplet.OPEN);
    } else {
      g.arc(centerPositionX2, centerPositionY2, (radio2+r)*2, (radio2+r)*2,
        PApplet.TWO_PI+(-winkel+gearWinkel),
        PApplet.TWO_PI+(winkel+gearWinkel),
        PApplet.OPEN);
    }
    
    
    float umf1 = PApplet.TWO_PI*radio1;
    float a = (tp11x-tp21x)*(tp11x-tp21x) + (tp11y-tp21y)*(tp11y-tp21y);
    float l = PApplet.sqrt(a);
    
    if (l<=0) return;
    if (Float.isNaN(l)) {
      return;
    }
    
    float t = PApplet.map(
      ((rotationAngle* PApplet.abs(rotationSpeed1))/PApplet.TWO_PI) * umf1, // teil des Umfangs
      0, umf1,
      0, umf1/l); // auf 
    while (t>1) {
      t = t-1;
    }
    
    // TODO I dont know when to do that:
    if (rotationSpeed1<0)
      t = 1-t;
    
    g.fill(50,105,60);
    g.noStroke();
    int dc = (int)(l / 15);
    for (int i = 0; i < dc; i++) {
      float tt = (t+((float)i/dc));
      if (tt>1) tt = tt-1;
      float px = tp12x + tt*(tp22x-tp12x);
      float py = tp12y + tt*(tp22y-tp12y);
      g.ellipse(px, py, 10,10);
    }
    for (int i = 0; i < dc; i++) {
      float tt = ((1-t)+((float)i/dc));
      if (tt>1) tt = tt-1;
      float px = tp11x + tt*(tp21x-tp11x);
      float py = tp11y + tt*(tp21y-tp11y);
      g.ellipse(px, py, 10,10);
    }
  }
}
