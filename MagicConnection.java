
class MagicConnection extends GearConnection {
  float ratio;
  
  public MagicConnection(Gear other, Gear g, Bruch ratio) {
    super(other, g);
    g.showRatio=false;
    this.ratio = ratio.value();
  }
  public void update() {
    //if (this.g.getTeethCount()==0) return;
    float speed = this.other.getSpeed() * this.ratio;
    this.g.setSpeed(speed);
  }
}
