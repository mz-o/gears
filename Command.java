import processing.core.PGraphics;
import java.util.ArrayList;

class Param {
  String name;
  Input input;
  
  float nameWidth;
  
  Param(String name, Input inp) {
    this.name = name;
    this.input = inp;
  }
  
  void draw(PGraphics g, float x, float y) {
    g.fill(190);
    g.textSize(24);
    g.textAlign(PGraphics.LEFT, PGraphics.TOP);
    String s = this.name + ": ";
    nameWidth = g.textWidth(s);
    g.text(s, x, y);
    this.input.draw(g, x+nameWidth, y);
  }
  
  void start() {
    input.reset();
  }
  
  boolean handleKey(char key, int keyCode) {
    return this.input.handleKey(key, keyCode);
  }
  
  boolean handleClick(int mx, int my) {
    mx -= (int)nameWidth;
    if (mx < 0) return false;
    return this.input.handleClick(mx, my);
  }
  
  String getValue() {
    return this.input.getValue();
  }
  
}

class Command {
  String name;
  ArrayList<Param> params = new ArrayList<Param>();
  String[] paramValues;
  
  int i = 0;
  
  Command(String name) {
    this.name = name;
  }
  
  Command addParam(String name, Input inp) {
    this.params.add(new Param(name, inp));
    return this;
  }
  
  String compileCommand() {
    String cmd = this.name;
    if (this.paramValues != null) {
      cmd += " " + String.join(" ", this.paramValues);
    }
    this.reset();
    return cmd;
  }
  
  void reset() {
    this.i = 0;
    this.paramValues = null;
  }
  
  void appendParam(String p) {
    this.initParams();
    if (this.paramValues == null) {
      return;
    }
    this.paramValues[this.i] = p;
    this.i++;    
  }
  
  void initParams() {
    if (this.paramValues == null && this.params.size() > 0) {
      this.paramValues = new String[this.params.size()];
    }
  }
  
  boolean needParam() {
    this.initParams();
    if (this.paramValues == null) return false;
    return this.i < this.paramValues.length;
  }
  
  Param getCurrentParam() {
    if (!this.needParam()) return null;
    return this.params.get(this.i);
  }
  
}
