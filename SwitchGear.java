import processing.core.PVector;
import processing.core.PGraphics;

class SwitchGear extends Gear {
  boolean state = false;
  Bruch ration;
  Gear reference;
  
  int referenceTurns = -1;
  float referenceRot = 0;
  
  
  public SwitchGear(int teethCount, PVector pos, int color, Gear reference, Bruch b) {
    super(teethCount, pos, color);
    this.reference = reference;
    this.ration = b;
  }
  
  protected int referenceTurns() {
    return (int)((Gear.rotation-this.referenceRot)/(PGraphics.TWO_PI/(this.reference.getSpeed()*this.ration.value())));
  }
  
  protected boolean referenceTurned() {
    return this.referenceTurns() != this.referenceTurns;
  }
  
  protected void drawMe(PGraphics g) {
    super.drawMe(g);
    
    if (this.referenceTurned()) {
      this.state = false;
    }
    if (this.newTurnCompleted()) {
      this.state = true;
      //this.referenceRot = Gear.rotation;
    }
    
    float r = this.getRadius();
    
    g.pushMatrix();
    g.strokeWeight(2);
    g.rectMode(PGraphics.CORNER);
    g.translate(this.p.x, this.p.y);
    g.rotate( this.speed * Gear.rotation + PGraphics.PI);
    g.stroke(this.color);
    g.line(0, 0, 0, r+Gear.teeth_height+20);
    g.popMatrix();
    
    g.fill(this.state ? this.color : 0);
    g.rect(this.p.x, this.p.y-r-Gear.teeth_height-20, 20, 20);
    
    float rot = Gear.rotation;
    float speed = this.getSpeed();
    
    int turns = this.turns();
    
    this.referenceTurns = this.referenceTurns();
  }
  
  
 
  
}
