class StickyGearConnection extends GearConnection {
  public StickyGearConnection(Gear other, Gear g) {
    super(other, g);
  }
  public void update() {
    this.g.setPosition(this.other.getPosition());
    this.g.setSpeed(this.other.getSpeed());
  }
}
