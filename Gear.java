import processing.core.PVector;
import processing.core.PGraphics;
import processing.core.PApplet;
import java.util.ArrayList;

class Gear {
  static float rotation = 0;
  static PApplet app;
  
  final static float teeth_height = 8; //18;
  
  protected PVector p;
  float speed = 0;
  int teethCount = 0;
  int color = 0;
  int lastTurns = -1;
  
  ArrayList<GearConnection> connections;
  GearConnection from = null;
  
  public boolean showRatio = true;
  
  class ChainedGearConnection extends ChainedGearConnectionBase {
    public ChainedGearConnection(Gear other, Gear g) {
      super(other, g, false);
    }
  }
  
  class CrossChainedGearConnection extends ChainedGearConnectionBase {
    public CrossChainedGearConnection(Gear other, Gear g) {
      super(other, g, true);
    }
  }
  
  
  public Gear(int teethCount, PVector pos, int color) {
    this.p = pos;
    this.teethCount = teethCount;
    this.connections = new ArrayList<GearConnection>();
    this.color = color;
  }
  
  public Gear() {
    this(10);
  }
  
  public Gear(int teethCount, PVector pos) {
    this(teethCount, pos, 0x60ffffff);
  }
  
  public Gear(int teethCount) {
    this(teethCount, new PVector(0,0));
  }
  
  public Gear(int teethCount, int color) {
    this(teethCount, new PVector(0,0), color);
  }
  
  
  public void setSpeed(float speed) {
    this.speed = speed;
  }
  
  public float getSpeed() {
    return this.speed;
  }
  
  public void noRatio() {
    this.showRatio = false;
  }
  
  public void connectTouching(Gear g) {
    g.from = new TouchingGearConnection(this, g);
    this.connections.add(g.from);
  }
  public void connectChained(Gear g) {
    g.from = new ChainedGearConnection(this, g);
    this.connections.add(g.from);
  }
  public void connectCrossChained(Gear g) {
    g.from = new CrossChainedGearConnection(this, g);
    this.connections.add(g.from);
  }
  public void connectSticky(Gear g) {
    g.from = new StickyGearConnection(this, g);
    this.connections.add(g.from);
  }
  public void connectMagic(Gear g, Bruch ratio) {
    g.from = new MagicConnection(this, g, ratio);
    this.connections.add(g.from);
  }
  public void connectNone(Gear g) {
    g.from = new NoGearConnection(this, g);
    this.connections.add(g.from);
  }
  
  
  public void connectTouching(StickyTwins t) {
    this.connectTouching(t.getGear1());
  }
  public void connectChained(StickyTwins t) {
    this.connectChained(t.getGear1());
  }
  public void connectCrossChained(StickyTwins t) {
    this.connectCrossChained(t.getGear1());
  }
  
  
  void disconnect(GearConnection c) {
    this.connections.remove(c);
  }
  
  void disconnect() {
    this.connections.clear();
  }
  
  
  public int getTeethCount() {
    return this.teethCount;
  }
  
  public void setTeethCount(int tc) {
    this.teethCount = tc;
  }
  
  public float getRadius() {
    return this.teethCount * 5;
  }
  
  public float getOuterRadius() {
    return this.getRadius() + Gear.teeth_height;
  }
  
  public PVector getPosition() {
    return this.p;
  }
  
  public void setPosition(PVector p) {
    if (this.from != null && this.from instanceof TouchingGearConnection) {
      this.p = new PVector(40,40);
    }
    
    this.p = p;
  }
  
  void setColor(int c) {
    this.color = c;
  }
  
  
  public void draw(PGraphics g) { //PGraphics
    
    for (GearConnection c : this.connections) {
      c.update();
    }
    
    for (GearConnection c : this.connections) {
      c.drawBefore(g);
    }
    
    this.drawMe(g);
    
    for (GearConnection c : this.connections) {
      c.drawGears(g);
    }
    
    for (GearConnection c : this.connections) {
      c.drawAfter(g);
    }
    
    this.lastTurns = this.turns();
  }
  
  protected void drawMe(PGraphics g) {
    float radio = this.getRadius();
    float centerPositionX = this.p.x;
    float centerPositionY = this.p.y;
    float teethHeight = Gear.teeth_height;
    float rotationAngle = Gear.rotation;
    
    int numberOfTeeth = this.teethCount; 
    float teethAngle=PApplet.TWO_PI/numberOfTeeth;
    float teethWidth=PApplet.sin(teethAngle/2)*radio; 
    float lineY=PApplet.cos(teethAngle/2)*radio+teethHeight;
    g.pushMatrix();
    g.translate(centerPositionX, centerPositionY);
    g.rotate( this.speed*rotationAngle);
    
    //int c = 0x60ff0000;
    //int c = 0x60ffffff;
    int c = this.color;
    
    g.fill(c);
    
    int dc = Gear.app.lerpColor(c, 0x66ffffff, 0.8f);
    
    g.noStroke();
    for (int i=0; i<numberOfTeeth; i++)
    {  
      if (i==numberOfTeeth-1) g.fill(dc);
      else g.fill(c);
      g.rotate(teethAngle); 
      g.quad(
      -3*teethWidth/4, -lineY+teethHeight +2,
      teethWidth/2, -lineY+teethHeight +1,
      teethWidth/4 -1, -lineY +1,
      -teethWidth/2 -1, -lineY +2
      );
    }
    
    g.fill(c);
    g.noStroke();
    g.ellipse(0, 0, 2*(-lineY+teethHeight), 2*(-lineY+teethHeight)) ;
    
    
    
    g.fill(0);
    g.noStroke();
    g.circle(0, 0, 20);//Shaft
    g.rectMode(PApplet.CENTER);
    g.rect(0, -10, 5, -100/15);
    g.ellipse(0, -0.85f*radio, radio/15, radio/15);
    
    
    g.popMatrix();
    
    g.textAlign(PApplet.CENTER, PApplet.TOP);
    g.text(""+this.turns(), centerPositionX, centerPositionY - radio);
  }

  
  protected void drawMyRatio(PGraphics g, Bruch b) {
    PVector pos = this.getPosition();
    //b.kuerzen();
    int c = g.color(255);
    b.draw(g, new PVector(pos.x-1, pos.y-1), c, 28);
    c = g.color(0);
    b.draw(g, new PVector(pos.x+1, pos.y+1), c, 28);
    c = g.color(200, 30,25);
    b.draw(g, pos, c, 28);
  }
  
  // startZ ist hier mit Vorzeichen!
  public void drawRatios(PGraphics g, Bruch start) {
    if (this.showRatio) {
      this.drawMyRatio(g, start);
    }
    PVector pos = this.getPosition();
    g.textAlign(PApplet.CENTER, PApplet.CENTER);
    g.textSize(18);
    for (GearConnection c : this.connections) {
      Gear gear = c.getGear2();
      if (!gear.showRatio || c instanceof StickyGearConnection) {
        gear.drawRatios(g, start);
      } else {
        PVector pos2 = gear.getPosition();
        PVector mid = PVector.lerp(pos, pos2, 0.5f);
        int z = this.getTeethCount() * this.signInt(gear.getSpeed()*this.getSpeed());
        int n = gear.getTeethCount();
        Bruch b = new Bruch(z, n);
        g.fill(100,200,0);
        String s = b.text() + "\n";
        b.kuerzen();
        s += b.text();
        g.text(s, mid.x, mid.y);
        //Bruch nb = start.clone();
        b.mult(start);
        b.kuerzen();
        gear.drawRatios(g, b);
      }
      
    }
    
  }
  
  /*String reduced(int zaehler, int nenner) {
    int z=PApplet.abs(zaehler);
    int n=PApplet.abs(nenner);
    int r=z % n;
     
    while(r>0){
      z = n;
      n = r;
      r = z % n;
    }
    //Kürzen des Bruches
    zaehler /= n;
    nenner /= n;
      return this.bruch(zaehler, nenner);
  }
  
  String bruch(int zaehler, int nenner) {
    return this.signString((float)zaehler/nenner) + PApplet.abs(zaehler) + "/" + PApplet.abs(nenner);
  }
  
  String signString(float n) {
    return n > 0 ? "+" : "–";
  }
  
  float sign(float n) {
    return n > 0 ? 1.0f : -1.0f;
  }
  */
  int signInt(float n) {
    return n > 0 ? 1 : -1;
  }
  
  int turns() {
    return PApplet.abs((int)(Gear.rotation/(PGraphics.TWO_PI/this.getSpeed())));
  }
  
  boolean newTurnCompleted() {
    return this.turns() != this.lastTurns;
  }
  
  
}
