import processing.core.PVector;

class StickyTwins {
  Gear g1;
  Gear g2;
  
  StickyTwins(int count1, int count2, PVector pos, int color) {
    this.g1 = new Gear(count1, pos, color);
    this.g2 = new Gear(count2, color);
    g1.connectSticky(g2);
  }
  
  StickyTwins(int count1, int count2, PVector pos) {
    this(count1, count2, pos, 0x66ffffff);
  }
  
  Gear getGear1() {
    return this.g1;
  }
  
  Gear getGear2() {
    return this.g2;
  }
  
  
  public void connectTouching(Gear g) {
    this.g2.connectTouching(g);
  }
  
  public void connectChained(Gear g) {
    this.g2.connectChained(g);
  }
  
  public void connectCrossChained(Gear g) {
    this.g2.connectCrossChained(g);
  }
  
  
  public void connectTouching(StickyTwins t) {
    this.connectTouching(t.getGear1());
  }
  public void connectChained(StickyTwins t) {
    this.connectChained(t.getGear1());
  }
  public void connectCrossChained(StickyTwins t) {
    this.connectCrossChained(t.getGear1());
  }

  
}
