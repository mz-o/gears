

//Simple Gears(visual simulation test code).
//Built with Processing 2.03 by Adrian Fernandez. Miami, FL. USA. (07-31-2014).

// innere tangenten https://www.youtube.com/watch?v=ApEKZ4AfdB4
//             https://www.youtube.com/watch?v=KGtn1VgKIoI
// äußere tangenten https://www.youtube.com/watch?v=cwnZwPZcPxA&list=PLFQhZXGWuTPX3Aw5SfPlHCliVsBt5ZOLR&index=77
/*


TODO Anderes Zahnrad als antrieb definieren (es darf aber nur eines pro antrieb-einheit sein.
     einige verbindungen müssen dann umgekehrt werden!

TODO magig connection needs param bruch
*/

final int minNumberOfTeeth=3;
final int maxNumberOfTeeth=40;
float r = 0;
ArrayList<Gear> drivers;
ArrayList<Gear> allGears;
String timingMethod = "revolution";
int keyTimeer = 0;
boolean receivingParam = false;
String commandString = "";
TimerListener keyTimerListener = null;
Command currentCommand = null;
Param currentParam = null;
//String paramValue = "";
float startMillis = millis();
int revolutionsPerMinute = 10;
//Gear selectedGear = null;
Gear[] selectedGears = new Gear[2];
PVector selDist = null;
float topSpace = 40;
InputListener inputListener;

void setup()
{
  size(1350, 690);  
  frameRate(60);
  textAlign(CENTER);
  textSize(18);
  
  drivers = new ArrayList<Gear>();
  allGears = new ArrayList<Gear>();
   
  Gear.app = this;
  Timer.app = this;
  Button.app = this;
  
  inputListener = new InputListener() {
    void enter() {
      currentCommand.appendParam(currentParam.getValue());
      currentParam = null;
      if (currentCommand.needParam()) {
        currentParam = currentCommand.getCurrentParam();
        currentParam.start();
      } else {
        String cmdString = currentCommand.compileCommand();
        currentCommand = null;
        executeCommand(cmdString);
      }
    }
  };
    
  ButtonGroup.add(new ButtonGroup("Verbindung"))
    .add(new Button("cross.svg", "connect x", 'x'))
    .add(new Button("chain.svg", "connect c", 'c'))
    .add(new Button("touch.svg", "connect t", 't'))
    .add(new Button("magic.svg", "connect m", 'm'))
    .add(new Button("sticky.svg", "connect s",'s'))
    .add(new Button("none.svg", "connect none",'n'))
    ;
        
  ButtonGroup.add(new ButtonGroup("Timing"))
    .add(new Button("reset.svg", "reset"))
    ////.add(new Button("step.svg", "timing step"))
    .add(new Button("revolution.svg",  new Command("timing revolution").addParam("Umdrehungen/min", new TextInput(inputListener))))
    ;
    
  ButtonGroup.add(new ButtonGroup("Zahnräder"))
    .add(new Button("add.svg", new Command("rad").addParam("Zähne", new TextInput(inputListener)), '+'))
    .add(new Button("add2.svg", new Command("bruch").addParam("Bruch", new TextInput(inputListener)), '#'))
    .add(new Button("remove.svg", "delete"))
    .add(new Button("teeth.svg", new Command("teeth").addParam("Zähne", new TextInput(inputListener)), 'z'))
    .add(new Button("color.svg", new Command("color").addParam("Farbe", new ColorInput(inputListener)), 'f'))
    ;
  
  
  
  /*
  Bruch[] ratios = new Bruch[] {
    new Bruch(-8, 12),
    new Bruch(25,10),
    new Bruch(10,14),
    new Bruch(8,23)
  };
  Gear startGear = create(ratios, null);
  drivers.add(startGear);
  startGear.setSpeed(1);
  */
  
  executeCommand("bruch 6/24");
  executeCommand("bruch 1/2");
  
  executeCommand("rad 20");
  executeCommand("bruch 1/2");

}
  

Gear create(Bruch[] ratios, Gear lastGear, String connection) {
  Gear firstGear = null;
  
  //ArrayList<GearPlace> places = new ArrayList<GearPlace>();
  GearPlace place;
  
  float y = height / 2;
  float x = 0;
  
  for (Bruch b: ratios) {
    boolean isPositive = b.isPositive();
    
    // TODO if letzte zahnrad gleich dem nächsten ersten: kein neues zahnrad bauen sonder ndirekt verbinden
    
    Gear g1, g2;
    
    g1 = new Gear(b.zaehler());
    g2 = new Gear(b.nenner());
    g2.setPosition(new PVector(g2.getOuterRadius(), topSpace+g2.getOuterRadius()));
    
    if (connection == null) {
      if (isPositive) {
        g1.connectChained(g2);
      } else {
        g1.connectCrossChained(g2);
      }
    } else {
      if (connection.startsWith("touch") || connection.equals("verzahnt")) {
        g1.connectTouching(g2);
      } else if (connection.startsWith("chain") || connection.equals("kette")) {
        g1.connectChained(g2);
      } else if (connection.startsWith("cross") || connection.startsWith("kreuz")) {
        g1.connectCrossChained(g2);
      } else {
        g1.connectChained(g2); // damit sie auf jeden Fall verbunden sind
      }
    }
    
    if (firstGear == null) firstGear = g1;
    
    if (lastGear != null) {
      lastGear.connectSticky(g1);
      g1.setPosition(lastGear.getPosition());
      // fals g1 größer ist als lastGear, muss die position von lastGear neu berechnet werden
      if (g1.getTeethCount() > lastGear.getTeethCount()) {
        float d = g1.getOuterRadius() - lastGear.getOuterRadius();        
        PVector pos = lastGear.getPosition();
        pos.x += d;
        x += d*2;
        lastGear.setPosition(pos);
      }
    } else {
      // das ist eh das erste
      x += g1.getOuterRadius();
      g1.setPosition(new PVector(x, y));
      x += g1.getOuterRadius();
    }
    allGears.add(g1);
    
    if (lastGear != null) {
      if (g1.getTeethCount() > lastGear.getTeethCount()) {
        findPosition(g1, g2);
      } else {
        findPosition(lastGear, g2);
      }
    } else {
      findPosition(g1, g2);
    }
    allGears.add(g2);
    
    lastGear = g2;
  }
  return firstGear;
}


void findPosition(Gear g1, Gear g2) {
  PVector pos1;
  float r1;
  float r2 = g2.getOuterRadius();
  if (g1 != null) {
    pos1 = g1.getPosition();
    r1 = g1.getOuterRadius();
  } else {
    pos1 = new PVector(width/2, height/2);
    r1 = -r2; // hack
  }
  float r = (r1+r2)*0.9;
  for (int i=0; i < 10; i++) {
    PVector[] positions = {
      new PVector(pos1.x+r+20,      pos1.y                  ), // #>
      new PVector(pos1.x+r,      pos1.y-r            ), // #'
      new PVector(pos1.x+r,      pos1.y+r            ), // #,
      new PVector(pos1.x+1,        pos1.y-r+20           ), // ^
      new PVector(pos1.x+1,        pos1.y+r+20           ), // v
      new PVector(pos1.x-r,      pos1.y+r            ), // ,#
      new PVector(pos1.x-r,      pos1.y-r            ), // '#
      new PVector(pos1.x-r+20,      pos1.y                  ), // <#
    };
    for (PVector pos2: positions) {
      if (pos2.y<topSpace) pos2.y=topSpace;
      if (testPosition(pos2, r2)) {
        if (pos2.y<topSpace) pos2.y=topSpace;
        g2.setPosition(pos2);
        return;
      }
    }
    r += 10;
  }
}

boolean testPosition(PVector pos, float r) {
  float x1 = pos.x - r, y1 = pos.y - r, x2 = pos.x + r, y2 = pos.y + r;
  float gx1, gy1, gx2, gy2;
  if (x1 < 0 || x2 > width || y1 < topSpace || y2 > height) return false;
  for (Gear g: allGears) {
    PVector gpos = g.getPosition();
    float gr = g.getOuterRadius()*1.3;
    gx1 = gpos.x - gr;
    gy1 = gpos.y - gr;
    gx2 = gpos.x + gr;
    gy2 = gpos.y + gr;
    if (gx1 > x2 || gy1 > y2 || gx2 < x1 || gy2 < y1) continue;
    return false;
  }
  return true;
}


void revolutionsPerMinute(int rpm) {
  // 1 Umdr. pro Minute
  Gear.rotation += rpm*(TWO_PI/frameRate)/60;   //0.002;
}


void stepPerSecond(int fraction) {
  // Rärdchen alle Sekunde 1/60 weiter
  float m = (int)(millis()-startMillis)/1000;
  Gear.rotation = (TWO_PI/fraction) * m;
}


void draw() {
  background(40);
  PVector pos;
  for (Gear gr : drivers) {
    gr.draw(g);
    // roter kreis für antriebsrad
    fill(200, 10, 10);
    pos = gr.getPosition();
    circle(pos.x, pos.y, 20);
    // beschriftung
    gr.drawRatios(g, new Bruch(1,1));
  }
  for (Gear selectedGear: selectedGears) {
    if (selectedGear == null) continue;
    pos = selectedGear.getPosition();
    float r = selectedGear.getOuterRadius();
    stroke(255,255,10,70);
    strokeWeight(10);
    noFill();
    circle(pos.x, pos.y, r*2);
    if (selectedGear.from != null) {
      Gear other = selectedGear.from.getGear1();
      PVector p2 = other.getPosition();
      strokeWeight(2);
      circle(p2.x, p2.y, other.getOuterRadius()*2);
      if (!(selectedGear.from instanceof StickyGearConnection)) {
        float d = PVector.dist(pos, p2);
        float d1 = (r+5)/d;
        float d2 = (d-other.getOuterRadius()-1)/d;
        line(pos.x+d1*(p2.x-pos.x), pos.y+d1*(p2.y-pos.y), pos.x+d2*(p2.x-pos.x), pos.y+d2*(p2.y-pos.y));
      }
    }
  }
  if (currentCommand != null && currentParam != null) {
    g.fill(255, 50);
    g.noStroke();
    g.rectMode(CORNER);
    g.rect(0, height-50, width, 50);
    currentParam.draw(g, 10, height-40);
  }
  ButtonGroup.paint_all(g);
  if (timingMethod.equals("step")) {
    stepPerSecond(60);
  } else if (timingMethod.equals("revolution")) {
    revolutionsPerMinute(revolutionsPerMinute);
  }
  Timer.tick();
}

boolean executeCommand(Command c) {
  if (c == null) {
    return false;
  }
  currentCommand = c;
  currentCommand.reset();
  if (!currentCommand.needParam()) {
    String commandString = currentCommand.compileCommand();
    if (commandString != null) {
      currentCommand = null;
      executeCommand(commandString);
    }
  } else {
    currentParam = currentCommand.getCurrentParam();
    currentParam.start();
  }
  return true;
}

void selectNothing() {
  selectedGears[0] = null;
  selectedGears[1] = null;
}

void select(Gear g, boolean clear) {
  if (clear) {
    selectedGears[0] = g;
    selectedGears[1] = null;
    return;
  }
  if (selectedGears[0] == null) selectedGears[0] = g;
  if (selectedGears[1] == null) selectedGears[1] = g;
}

boolean selectionFull() {
  return selectedGears[0] != null && selectedGears[1] != null;
}

Gear getOneSelected() {
  return selectedGears[0];
}

boolean exactlyOneSelected() {
  return selectedGears[0] != null ^ selectedGears[1] != null;
}

void mousePressed(processing.event.MouseEvent event) {
  if (executeCommand(ButtonGroup.hit_test(mouseX, mouseY))) return;
  if (currentParam != null) {
    if (mouseX > 0 && mouseX < width && mouseY > height-50 && mouseY < height) {
      if (currentParam.handleClick(mouseX-10, mouseY)) return;
    }
  }
  PVector mouse = new PVector(mouseX, mouseY);
  for (Gear g: drivers) {
    Gear sg = gearHitTest(g, mouse);
    if (sg != null) {
      select(sg, !(event.isShiftDown() || event.isControlDown()) );
      selDist = PVector.sub(mouse, sg.getPosition());
      return;
    }
  }
  selectNothing();
}

Gear gearHitTest(Gear parent, PVector point) {
  ArrayList<Gear> sticky = new ArrayList<Gear>();
  
  for (GearConnection c: parent.connections) {
    Gear g2 = c.getGear2();
    if (c instanceof StickyGearConnection) {
      sticky.add(g2);
    }
  }
  if (sticky.size()>0) {
    sticky.add(parent);
    Gear sg = null;
    float r = 0;
    for (Gear g: sticky) {
      if (isPointOnGear(point, g)) {
        if (sg == null || g.getRadius() < r) {
          r = g.getRadius();
          sg = g;
        }
      }
    }
    if (sg != null) return sg;
  }
  
  if (isPointOnGear(point, parent)) {
    return parent;
  }
  
  for (GearConnection c: parent.connections) {
    Gear g2 = c.getGear2();
    Gear sg = gearHitTest(g2, point);
    if (sg != null) return sg;
  }
  
  return null;
}

boolean isPointOnGear(PVector point, Gear gear) {
  PVector pos = gear.getPosition();
  float r = gear.getOuterRadius();
  return PVector.dist(pos, point) < r;
}



void mouseReleased() {
}

void mouseDragged() {
  if (exactlyOneSelected()) {
    PVector mouse = new PVector(mouseX, mouseY);
    Gear movingGear = getOneSelected();
    if (movingGear.from instanceof StickyGearConnection) {
      movingGear = movingGear.from.getGear1();
    }
    movingGear.setPosition(PVector.sub(mouse, selDist));
  }
}



void keyPressed() {
  if (keyCode == DELETE) {
    executeCommand("delete");
    return;
  }
  if (currentCommand != null && currentParam != null) {
    if (keyCode == ESC) {
      currentCommand.reset();
      currentCommand = null;
      currentParam = null;
      key = 0;
      keyCode = 0;
      return;
    }
    if (keyCode == ENTER) {
      inputListener.enter();
      return;
    }
    if (currentParam != null) {
      if (currentParam.handleKey(key, keyCode)) return;
    }
    return;
  }
  if (executeCommand(ButtonGroup.key_hit_test(key))) return;
}

void resetCommand() {
  receivingParam = false;
  commandString = "";
}

void executeCommand(String command) {
  println("CMD " + command);
  
  Gear selectedGear = getOneSelected();
  
  if (command.equals("delete") && exactlyOneSelected()) {
    if (!selectedGear.connections.isEmpty()) return;
    drivers.remove(selectedGear);
    allGears.remove(selectedGear);
    GearConnection fromConnection = selectedGear.from;
    if (fromConnection != null) {
      Gear g1 = fromConnection.getGear1();
      g1.disconnect(fromConnection);
    }
    selectNothing();
    return;
  }
  
  if (command.startsWith("timing ")) {
    command = command.substring(7);
    if (command.startsWith("revolution ")) {
      try {
        command = command.substring(11);
        int number = Integer.parseInt(command);
        revolutionsPerMinute = number;
      } catch (NumberFormatException ex) {
      }
      timingMethod = "revolution";
    } else {
      timingMethod = command;
    }
    executeCommand("reset");
    return;
  }
  
  if (command.startsWith("teeth ") && exactlyOneSelected()) {
    try {
      command = command.substring(6);
      int number = Integer.parseInt(command);
      number = limitTeethCount(number);
      selectedGear.setTeethCount(number);
    } catch (NumberFormatException ex) {
    }
    return;
  }
  
  if (command.equals("reset")) {
    startMillis = millis();
    Gear.rotation = 0;
    return;
  }
  
  // add Gear
  if (command.startsWith("bruch ")) {
    command = command.substring(6);
    addCommand2(command.split(" "));
    return;
  }
  if (command.startsWith("rad ")) {
    command = command.substring(4);
    addCommand(command.split(" "));
    return;
  }
  
  // change connection
  if (command.startsWith("connect ")) {
    command = command.substring(8);
    String[] a = command.split(" ");
    if (exactlyOneSelected()) {
      // old implementation
      changeConnectionCommand(a[0], a.length > 1 ? a[1] : null);
    } else if (selectionFull()) {
      connectTwoGears(selectedGears[0], selectedGears[1], a[0], a.length > 1 ? a[1] : null);
    }
    return;
  }
  
  // change color
  if (command.startsWith("color ") && exactlyOneSelected()) {
    command = command.substring(6);
    int c;
    try {
      c = Integer.parseInt(command, 10);
      c = color(red(c), green(c), blue(c), 0x60);
    } catch (NumberFormatException ex) {
      return;
    }
    selectedGear.setColor(c);
    return;
  }
  
}

void connectTwoGears(Gear g1, Gear g2, String con, String p) {
  // wenn sie schon verbunden sind, dann sollten sie so verbunden sein:
  // g1->g2
  // g1.connections[?].getGear2() == g2
  // g2.from.getGear1() == g1;
  
  
  if (g2.from != null && g2.from.getGear1() == g1) {
    println("miteinander verbunden normale ausrichtung -> connection ändern");
  } else if (g1.from != null && g1.from.getGear1() == g2) {
    println("miteinander verbunden aber verdrehte ausrichtung -> g1 und g2 tauschen, dann connection ändern");
    Gear g = g1;
    g1 = g2;
    g2 = g;
  } else {
    println("nicht miteinander verbunden");
    if (g2.from != null && g1.from != null) {
      println("beide anderweitig verbunden -> exit");
      return;
    } else if (g1.from == null && g2.from != null) { // wenn beide null ist die reihenfolge des markierens bedeutsam
      println("g1 hat keinen from und sollte daher g2 sein");
      Gear g = g1;
      g1 = g2;
      g2 = g;
    }
    // TODO kreis erkennen!
    if (isCircular(g1,g2) || isCircular(g2,g1)) { // muss man die auch tauschen?
      println("zirkuläre verbindung! --> exit");
      return;
    }
    //Gear root 
  }
  
  println("connection ändern!");
  
  if (g2.from != null) {
    g1.disconnect(g2.from);
    g2.from = null;
    drivers.add(g2); // steht jetzt erstmal alleine
    g2.setSpeed(1);
  }
  doConnection(g1, g2, con, p);
  if (g2.from != null) {
    drivers.remove(g2);
  }
  
  return;  
  /*
  
  // wir können keine verbindung machen wenn beide ein from gesetzt haben
  if (g2.from==null) {
    
    
    // not connected
    // TODO richtung herausfinden und ensprechend tauschen
    println("g2 not connected at all");
  } else {
    // hier sind g1 und g2 miteinander verbunden, wir lösen die verbindung und setzen sie ggf. neu:
  }
  
  */
}

boolean isCircular(Gear g1, Gear g2) {
  if (g1 == g2) return true;
  // g1 bleibt constant
  if (g2.from == null) return false;
  Gear next = g2.from.getGear1();
  return isCircular(g1, next);
}

void doConnection(Gear g1, Gear g2, String con, String p) {
  if (con.equals("t")) {
    g1.connectTouching(g2);
    return;
  }
  if (con.equals("c")) {
    g1.connectChained(g2);
    return;
  }
  if (con.equals("x")) {
    g1.connectCrossChained(g2);
    return;
  }
  if (con.equals("s")) {
    g1.connectSticky(g2);
    return;
  }
  if (con.equals("m")) {
    Bruch b = new Bruch(1,1); // TODO
    g1.connectMagic(g2, b);
    return;
  }  
}

void changeConnectionCommand(String con, String p) {
  // die verbinding wo der antrieb her kommt, denn die ist eindeutig!
  if (!exactlyOneSelected()) return;
  Gear selectedGear = getOneSelected();
  GearConnection c = selectedGear.from;
  if (c == null) return;
  Gear g1 = c.getGear1();
  g1.disconnect(c);
  selectedGear.from = null;
  if (con.equals("none")) {
    drivers.add(selectedGear); // steht jetzt alleine
    selectedGear.setSpeed(1);
    return;
  }
  doConnection(g1, selectedGear, con, p);  
  if (selectedGear.from != null) {
    drivers.remove(selectedGear);
  }
}



void addCommand(String[] cmd) {
  int teeth = 6;
  if (cmd.length > 0) {
    try {
      teeth = PApplet.abs(Integer.parseInt(cmd[0]));
    } catch (NumberFormatException ex) {
      return;
    }
  }
  
  Gear last;
  Gear selectedGear = getOneSelected();
  if (selectedGear != null) {
    last = selectedGear;
  } else if (allGears.size() > 0) {
    last = allGears.get(allGears.size()-1);
  } else {
    last = null;
  }
  // connection TODO
  Gear g = newGear(teeth, last, null);
  
  
}

Gear newGear(int teeth, Gear g1, String connection) {
  println(teeth, g1, connection);
  
  Gear g2;
  g2 = new Gear(teeth);
  g2.setPosition(new PVector(width/2, height/2));
  //g1.connectNone(g2);
  if (drivers.size() == 0) {
    g2.setSpeed(1);
  }
  allGears.add(g2);
  drivers.add(g2); // nur wenn keine connection
  g2.setSpeed(1);
  findPosition(g1, g2);
  return g2;
}


void addCommand2(String[] cmd) {
  String connection = null;
  String[] sp = cmd[0].split("/");
  if (sp.length < 2) {
    return;
  }
  if (cmd.length >= 2) {
    connection = cmd[1];
  }
  /*int z = limitTeethCount(PApplet.abs(Integer.parseInt(sp[0].trim())));
  int n = limitTeethCount(PApplet.abs(Integer.parseInt(sp[1].trim())));*/
  int z, n;
  try {
    z = PApplet.abs(Integer.parseInt(sp[0].trim()));
    n = PApplet.abs(Integer.parseInt(sp[1].trim()));
  } catch (NumberFormatException ex) {
    return;
  }
  Bruch b = new Bruch(z, n);
  if (b.zaehler() < 6 || b.nenner() < 6) {
    int m = max(ceil(6.0/z), ceil(6.0/n));
    b.erweitern(m);
  }
  Gear last;
  Gear selectedGear = getOneSelected();
  if (selectedGear != null) {
    last = selectedGear;
  } else if (allGears.size() > 0) {
    last = allGears.get(allGears.size()-1);
  } else {
    last = null;
  }
  Bruch[] blist = {b};
  Gear neu = create(blist, last, connection);
  if (drivers.size() == 0) {
    drivers.add(neu);
    neu.setSpeed(1);
  }
}


int limitTeethCount(int c) {
  if (c < 6) c = 6;
  if (c > 60) c = 60;
  return c;
}
