import java.util.ArrayList;
import processing.core.PApplet;

class Timer {
  static PApplet app;
  
  static class TimerDing {
    TimerListener l;
    int t;
    TimerDing(TimerListener l, int t) {
      this.l = l;
      this.t = t;
    }
  }
  
  static ArrayList<TimerDing> listeners = new ArrayList<TimerDing>(); 
  
  static void tick() {
    int m = app.millis();
    ArrayList<TimerDing> remove = new ArrayList<TimerDing>();
    for (TimerDing td : listeners) {
      if (m >= td.t) {
        td.l.handle();
        remove.add(td);
      }
    }
    for (TimerDing td : remove) {
      listeners.remove(td);
    }
  }
  
  static void clearTimeout(TimerListener listener) {
    for (TimerDing td : listeners) {
      if (td.l == listener) {
        listeners.remove(td);
        return;
      }
    }
  }
  
  static void setTimeout(TimerListener listener, int interval) {
    listeners.add(new TimerDing(listener, app.millis()+interval));
  }
  
  
  
}
