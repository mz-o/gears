import processing.core.PGraphics;
import processing.core.PApplet;


abstract class GearConnection {
  protected Gear g;
  protected Gear other;
  public GearConnection(Gear other, Gear g) {
    this.g = g;
    this.other = other;
  }
  public Gear getGear1() {
    return this.other;
  }
  public Gear getGear2() {
    return this.g;
  }
  public void drawGears(PGraphics g) {
    this.g.draw(g);
  }
  abstract public void update();
  public void drawBefore(PGraphics g) {
  }
  public void drawAfter(PGraphics g) {
  }
}

class NoGearConnection extends GearConnection {
  public NoGearConnection(Gear other, Gear g) {
    super(other, g);
  }
  public void update() {
  }
  
}
