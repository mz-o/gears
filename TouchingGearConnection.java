import processing.core.PGraphics;
import processing.core.PVector;

class TouchingGearConnection extends GearConnection {
  public TouchingGearConnection(Gear other, Gear g) {
    super(other, g);
  }
  public void update() {
    if (this.g.getTeethCount()==0) return;
    float speed = -this.other.getSpeed() * ((float)this.other.getTeethCount() / this.g.getTeethCount());
    this.g.setSpeed(speed);
  }
  
  public void drawBefore(PGraphics g) {
    super.drawBefore(g);
    g.stroke(255,20);
    g.strokeWeight(5);
    PVector pos1 = this.getGear1().getPosition();
    PVector pos2 = this.getGear2().getPosition();
    g.line(pos1.x, pos1.y, pos2.x, pos2.y);
  }
  
}
