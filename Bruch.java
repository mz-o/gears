
import processing.core.PGraphics;
import processing.core.PApplet;
import processing.core.PVector;


class Bruch {
  int z, n;
  
  public Bruch(int z, int n) {
    this.z = z;
    this.n = n;
    
  }
  
  int zaehler() {
    return PApplet.abs(this.z);
  }
  
  int nenner() {
    return PApplet.abs(this.n);
  }
   
  void mult(Bruch b) {
    this.z *= b.z;
    this.n *= b.n;
  }
  
  void erweitern(int f) {
    this.z *= f;
    this.n *= f;
  }
  
  void kuerzen(int f) {
    if (this.z % f == 0 && this.n % f == 0) {
      this.z /= f;
      this.n /= f;
    }
  }
  
  void kuerzen() {
    int z = PApplet.abs(this.z);
    int n = PApplet.abs(this.n);
    int r = z % n;
     
    while(r>0){
      z = n;
      n = r;
      r = z % n;
    }
    //Kürzen des Bruches
    this.n /= n;
    this.z /= n;
  }
  
  void draw(PGraphics g, PVector pos, int c, int fs) {
    
    int n = this.nenner();
    int z = this.zaehler();
    float w;
    
    g.noStroke();
    g.fill(c);
    if (n == 1) {
      fs *= 1.5;
      g.textSize(fs);
      g.textAlign(PApplet.CENTER, PApplet.CENTER);
      String t = ""+z;
      w = g.textWidth(t);
      g.text(t, pos.x, pos.y-g.textDescent()*0.5f);
    } else {
      String nt = "" + n;
      String zt = "" + z;
      g.textSize(fs);
      w = PApplet.max(g.textWidth(nt), g.textWidth(zt));
      g.textAlign(PApplet.CENTER, PApplet.BOTTOM);
      g.text(zt, pos.x, pos.y);
      g.textAlign(PApplet.CENTER, PApplet.TOP);
      g.text(nt, pos.x, pos.y);
      g.strokeWeight(fs/8);
      g.stroke(c);
      g.line(pos.x-w/2, pos.y, pos.x+w/2, pos.y);
    }
    if (!isPositive()) {
      g.strokeWeight(fs/8);
      g.stroke(c);
      g.line(pos.x-w/2-fs/4, pos.y, pos.x-w/2-fs/4-fs*0.4f, pos.y);
    }
  }
  
  float value() {
     return (float)this.z / this.n; 
  }
  
  boolean isPositive() {
    return this.value() > 0 ? true : false;
  }
  
  String signString() {
    return this.isPositive() ? "" : "–";
  }
  
  int sign() {
    return this.isPositive() ? 1 : -1;
  }
  
  
  String text() {
    String result = this.zaehler() + "/" + this.nenner();
    if (!this.isPositive()) {
      result = "(-"+result+")";
    }
    return "×" + result;
  }
  
  public String toString() {
    return text();
  }
  
  public Bruch clone() {
    return new Bruch(this.z, this.n);
  }
  
}
