import processing.core.PVector;
import processing.core.PGraphics;
import processing.core.PApplet;
import processing.core.PShape;
import java.util.ArrayList;


class ButtonGroup {
  static ArrayList<ButtonGroup> groups = new ArrayList<ButtonGroup>();
  static void paint_all(PGraphics g) {
    float x = 0;
    float y = 0;
    for (ButtonGroup bg: groups) {
      bg.paint(g, x, y);
      x+=bg.getWidth();
      x+=5;
    }
  }
  static ButtonGroup add(ButtonGroup b) {
    groups.add(b);
    return b;
  }
  static Command hit_test(float mx, float my) {
    float x = 0;
    float y = 0;
    for (ButtonGroup bg: groups) {
      float w = bg.getWidth();
      float h = bg.getHeight();
      if (mx>x&&mx<x+w && my>y+h&&my<y+h+Button.size) {
        return bg.hitTest(mx-x, my-y-h);
      }
      x+=w;
      x+=5;
    }
    return null;
  }
  static Command key_hit_test(char key) {
    for (ButtonGroup bg: groups) {
      Command c = bg.keyHitTest(key);
      if (c != null) return c;
    }
    return null;
  }
  
  
  
  
  String title;
  
  ButtonGroup(String title) {
    this.title = title;
  }
  
  ArrayList<Button> buttons = new ArrayList<Button>();
  
  ButtonGroup add(Button b) {
    buttons.add(b);
    return this;
  }
  
  Command hitTest(float mx, float my) {
    float x = 0;
    float y = 0;
    for (Button b: buttons) {
      if (mx>x&&mx<x+Button.size&&my>y&&my<y+Button.size) {
        return b.command;
      }
      x+=Button.size;
    }
    return null;
  }
  
  Command keyHitTest(char key) {
    for (Button b: buttons) {
      if (b.actionKey == key) {
        return b.command;
      }
    }
    return null;
  }
  
  void paint(PGraphics g, float x, float y) {
    g.textAlign(PGraphics.CENTER, PGraphics.TOP);
    g.textSize(12);
    g.rectMode(PGraphics.CORNER);
    g.fill(255, 255, 255);
    g.stroke(128, 128, 128);
    g.strokeWeight(1);
    float w = this.getWidth();
    float h = this.getHeight();
    g.rect(x, y, w, h);
    g.fill(0);
    g.text(this.title, x+w/2, y);
    
    float xx = x;
    float yy = y+h;
    for (Button b: this.buttons) {
      b.paint(g, xx, yy);
      xx+=Button.size;
    }
    
  }
  
  float getWidth() {
    return this.buttons.size() * Button.size;
  }
  float getHeight() {
    return 16;
  }
  
}
