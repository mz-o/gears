import processing.core.PVector;
import processing.core.PGraphics;
import processing.core.PApplet;
import processing.data.IntDict;
import java.util.HashMap;
import java.util.Map;


interface InputListener {
  void enter();
}

abstract class Input {
  String paramValue;
  
  InputListener inputListener; 
  
  Input(InputListener inputListener) {
    this.inputListener = inputListener;
  }
  
  abstract void draw(PGraphics g, float x, float y );
  void reset() {
    this.paramValue = "";
  }
  boolean handleKey(char key, int keyCode) {
    return false;
  }
  boolean handleClick(int mx, int my) {
    return false;
  }
  
  
  boolean isCommandKey(char k) {
    final char[] all = {
      'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
      'ä','ö','ü','ß',
      '1','2','3','4','5','6','7','8','9','0',
      '#',':','+','-',' ','/',
    };
    for (int i = 0; i < all.length; i++) {
      if (all[i] == k) return true;
    }
    return false;
  }
  
  String getValue() {
    return this.paramValue;
  }
  
  void simulateEnter() {
    inputListener.enter();
  }
  
}




class ColorInput extends Input {
  IntDict colors = new IntDict();
  String selected;
  
  ColorInput(InputListener inputListener) {
    super(inputListener);
    colors.set("rot",   0xffff3030);
    colors.set("türkis",0xff50fff0);
    colors.set("grün",  0xff50ff50);
    colors.set("gelb",  0xffffff50);
    colors.set("pink",  0xffff50ff);
    colors.set("blau",  0xff5050ff);
    colors.set("weiß",  0xffffffff);
  }
  
  void reset() {
    super.reset();
    this.selected = null;
  }
  
  void draw(PGraphics g, float x, float y) {
    float ox = 0;
    for (IntDict.Entry c: colors.entries()) {
      g.fill(c.value);
      if (selected != null && selected.equals(c.key)) {
        g.strokeWeight(2);
        g.stroke(255);
      } else {
        g.noStroke();
      }
      g.rect(x+ox,y, 40, 30);
      ox += 50;
    }
  }
  boolean handleClick(int mx, int my) {
    int i = (int)(mx / 50);
    if (i<0 || i>=colors.size()) return false;
    System.out.println(":: " + i);
    selected = colors.key(i);
    this.paramValue = ""+colors.value(i);
    simulateEnter();
    return true;
  }
}





class TextInput extends Input {
  int cursorTimer=0;
  boolean cursor = true;
  
  TextInput(InputListener inputListener) {
    super(inputListener);
  }
  
  void draw(PGraphics g, float x, float y) {
    g.fill(255);
    g.textSize(24);
    g.textAlign(PGraphics.LEFT, PGraphics.TOP);
    String s = paramValue;
    float l = g.textWidth(s);
    g.text(s, x, y);
    // Eingabemarke
    int m = Gear.app.millis();
    if (m>cursorTimer) {
      cursorTimer = m+500;
      cursor = !cursor; 
    }
    if (cursor) {
      g.stroke(255);
      g.strokeWeight(2);
      g.line(x+l, y, x+l, y+28);
    }
    
  }
  
  boolean handleKey(char key, int keyCode) {
    if (keyCode == PGraphics.BACKSPACE) {
      if (this.paramValue.length() > 0) {
        this.paramValue = this.paramValue.substring(0, this.paramValue.length()-1);
      }
      return true;
    }
    if (this.isCommandKey(key)) {
      this.paramValue += key;
      return true;
    }
    return false;
  }  
}
