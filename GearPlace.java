import processing.core.PVector;

class GearPlace {
  float r;
  PVector pos;
  
  GearPlace(float r, PVector pos) {
    this.r = r;
    this.pos = pos;
  }
  
  PVector getPosition() {
    return this.pos;
  }
  
}
